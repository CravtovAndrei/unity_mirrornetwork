﻿using Mirror;
using UnityEngine;

public class CustomNetworkManager : NetworkManager 
{
    public override void OnStartServer()
    {
        base.OnStartServer();
        
        
        NetworkServer.RegisterHandler<PlayerCreationMessage>(OnCreateCharacter);
        SpawnCubesOnServer();
        
    }


    public void SpawnCubesOnServer(int quantity = 50)
    {
        for (int i = 0; i < quantity; i++)
        {
           var cub = Instantiate(spawnPrefabs.Find(prefab => prefab.name == "Cube"));
           NetworkServer.Spawn(cub);
        }
    }


    public override void OnServerAddPlayer(NetworkConnection conn)
    {
        //base.OnServerAddPlayer(conn);

        /*
        var player = Instantiate(playerPrefab);
        NetworkServer.AddPlayerForConnection(conn,player);
        */

        var cub = Instantiate(spawnPrefabs.Find(prefab => prefab.name == "Cube"));
        Debug.Log("Shit");
        NetworkServer.Spawn(cub);
    }

    public override void OnClientConnect(NetworkConnection conn)
    {
        base.OnClientConnect(conn);
        
        var characterMessage = new PlayerCreationMessage
        {
            charType = PlayerCreationMessage.CharacterType.Human,
            name = "Andrew"
        };
        
        
        NetworkServer.SetClientReady(conn); 
        conn.Send(characterMessage);
    }


    void OnCreateCharacter(NetworkConnection connection, PlayerCreationMessage message)
    {
       var character = Instantiate(playerPrefab);

       NetworkServer.AddPlayerForConnection(connection, character);
       
    }
}
