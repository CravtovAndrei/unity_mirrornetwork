﻿using System;
using Mirror;
using UnityEngine;

public class NetworkPrefabRegistration : NetworkBehaviour 
{
    [SerializeField] GameObject[] _registeredPrefabs;

    [Command]
    public void CmdSpawnObject()
    {
        var obj = Instantiate(_registeredPrefabs[0]);
        NetworkServer.Spawn(obj);
    }

}
