﻿using Mirror;

public class PlayerCreationMessage : MessageBase 
{
   public CharacterType charType { get; set; } 
   public string name { get; set; }

   public enum CharacterType 
   {
      Human,
      Robot
   }
}
