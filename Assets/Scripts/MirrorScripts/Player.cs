﻿using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;

public class Player : NetworkBehaviour
{
    [SerializeField] float _speed = 5f;
    [Client]
    void Update()
    {
        if(!hasAuthority) return;
        
        transform.position += new Vector3(Input.GetAxis("Horizontal") * (_speed * Time.deltaTime), 0, Input.GetAxis("Vertical") * (_speed * Time.deltaTime));
    }

}
