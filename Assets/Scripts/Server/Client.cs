﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net;
using System.Net.Sockets;

public class Client : MonoBehaviour
{
    public static Client Instance;
    public static int dataBufferSize = 4096;

    public string IP = "127.0.0.1";

    public int port = 26950;
    public int myId = 0;
    public TCP tcp;

    delegate void PacketHandler(Packet packet);
    static Dictionary<int, PacketHandler> packetHandlers;

    void Awake()
    {
        if (Instance == null) Instance = this;
        else if(Instance != this) Destroy(this);
    }

    void Start()
    {
        tcp = new TCP(); 
        
        ConnectToServer();
    }

    public void ConnectToServer()
    {
        InitializeClientData();
        tcp.Connect(); 
    }

    public class TCP
    {
        public TcpClient socket;
        Packet receivedData;
        NetworkStream stream;
        byte[] receiveBuffer;

        public void Connect()
        {
           socket = new TcpClient
           {
               ReceiveBufferSize = dataBufferSize,
               SendBufferSize = dataBufferSize
           };

           receivedData = new Packet();
           
           receiveBuffer = new byte[dataBufferSize];

           socket.BeginConnect(Instance.IP, Instance.port, ConnectCallback, socket);
        }

        void ConnectCallback(IAsyncResult result)
        {
           socket.EndConnect(result); 
           
           if(!socket.Connected) return;

           stream = socket.GetStream();
           stream.BeginRead(receiveBuffer, 0, dataBufferSize, ReceiveCallback, null);
        }

        void ReceiveCallback(IAsyncResult result)
        {
            try
            {
                int byteLength = stream.EndRead(result);

                if (byteLength <= 0) return;


                byte[] data = new byte[byteLength];
                Array.Copy(receiveBuffer, data, byteLength);

                receivedData.Reset(HandleData(data));
                
                stream.BeginRead(receiveBuffer, 0, dataBufferSize, ReceiveCallback, null);
            }
            catch(Exception e)
            {
                Debug.Log(e.Message);
            }
        }

        bool HandleData(byte[] data)
        {
            int packetLength = 0;
            
            receivedData.SetBytes(data);

            if (receivedData.UnreadLength() >= 4)
            {
                packetLength = receivedData.ReadInt();
                
                if (packetLength <= 0) return true;
            }

            while (packetLength > 0 && packetLength <= receivedData.UnreadLength())
            {
                byte[] packedBytes = receivedData.ReadBytes(packetLength);
                
                ThreadManager.ExecuteOnMainThread((() =>
                {
                    using (Packet packet = new Packet(packedBytes))
                    {
                        int packetId = packet.ReadInt();
                        packetHandlers[packetId](packet);
                    }
                }));

                packetLength = 0;
                
                if (receivedData.UnreadLength() >= 4)
                {
                    packetLength = receivedData.ReadInt();
                    
                    if (packetLength <= 0) return true;
                }
            }

            if (packetLength <= 1) return true;

            return false;
        }

    }
    void InitializeClientData()
    {
        packetHandlers = new Dictionary<int, PacketHandler> {{(int) ServerPackets.welcome, ClientHandle.Welcome}};
        Debug.Log("Initialize clients packets.");
    }
}
